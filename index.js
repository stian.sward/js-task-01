const laptops = [
    {
        'id': 0,
        'manufacturer': 'ASUS',
        'model': 'Zenbook 14',
        'description': 'A very decent workstation for sending email and reading the news. Not loud at all and great battery life thanks to the superior AMD CPU with integrated graphics.',
        'price': 13999,
        'cpuManufacturer': 'AMD',
        'cpuModel': 'Ryzen 7 4700U',
        'ram': '16GB 3733MHz',
        'storage': '1TB PCIe NVMe',
        'gpu': 'NVIDIA GeForce MX350',
        'image': './images/asus-zenbook-14.jpg'
    },
    {
        'id': 1,
        'manufacturer': 'ASUS',
        'model': 'ProArt Studiobook One',
        'description': 'Officially the most powerful laptop in the world, this monster uses top-of-the-line DESKTOP components for processing, and will run circles around everything but the most extreme desktop computers.',
        'price': 90499,
        'cpuManufacturer': 'Intel',
        'cpuModel': 'Core i9-9980HK',
        'ram': '64GB 2666MHz',
        'storage': '1TB PCIe NVMe',
        'gpu': 'NVIDIA Quadro RTX 6000',
        'image': './images/asus-proart-studiobook-one.jpg'
    },
    {
        'id': 2,
        'manufacturer': 'ASUS',
        'model': 'ROG Mothership',
        'description': 'This is a weird little number. All the high-powered components are behind the screen, not underneath the keyboard. This makes for an interesting setup.',
        'price': 59999,
        'cpuManufacturer': 'Intel',
        'cpuModel': 'Core i9-9980HK',
        'ram': '32GB DDR4 2666MHz',
        'storage': '1.5TB PCIe NVMe',
        'gpu': 'NVIDIA RTX 2080',
        'image': './images/asus-rog-mothership.jpg'
    },
    {
        'id': 3,
        'manufacturer': 'HP',
        'model': 'ZBook 15v G5',
        'description': 'Loud AF, this jet plane of a computer will melt your thighs AND ruin your neatly combed hair.',
        'price': 18990,
        'cpuManufacturer': 'Intel',
        'cpuModel': 'Core i7-8750H',
        'ram': '16GB 2666MHz',
        'storage': '256GB SATA SSD',
        'gpu': 'NVIDIA Quadro P600',
        'image': './images/hp-zbook-15v-g5.jpg'
    }
];

/* Global variables */
var accountBalance = 10000;
var pay = 5000;
var hasBorrowed = false;
var currentLaptop = null;

/* Elements */
const elSelecter = document.getElementById('laptopsPicker');
const elLoanBtn = document.getElementById('loanBtn');
const elDepositBtn = document.getElementById('depositBtn');
const elWorkBtn = document.getElementById('workBtn');
const elPayAmount = document.getElementById('payAmount');
const elAccountBalance = document.getElementById('accountBalance');
const elBuyBtn = document.getElementById('buyBtn');

/* Set initial values */
elAccountBalance.innerText = accountBalance;
elPayAmount.innerText = pay;

/* Event listeners */
elSelecter.addEventListener('change', function(event) {
    displayLaptopInfo(elSelecter.value);
    currentLaptop = elSelecter.value;
    document.getElementById('laptopInfoBox').style.display = 'grid';
});

elWorkBtn.addEventListener('click', function(event) {
    pay += 100;
    elPayAmount.innerText = pay;
});

elDepositBtn.addEventListener('click', function(event) {
    accountBalance += pay;
    pay = 0;
    elAccountBalance.innerText = accountBalance;
    elPayAmount.innerText = 0;
});

elLoanBtn.addEventListener('click', function(event) {
    if (hasBorrowed) {
        alert("You can't have more than one loan at a time");
        return;
    }
    let loan = prompt('How much do you want to borrow?');
    if (!loan) {return;}
    if (loan > (accountBalance * 2)) {
        alert("You can't borrow more than twice your account balance");
        return;
    }
    accountBalance += parseInt(loan);
    elAccountBalance.innerText = accountBalance;
    hasBorrowed = true;
});

elBuyBtn.addEventListener('click', function(event) {
    if (accountBalance < laptops[currentLaptop].price) {
        alert("You can't afford that laptop!");
        return;
    }
    accountBalance -= laptops[currentLaptop].price;
    elAccountBalance.innerText = accountBalance;
    hasBorrowed = false;
    alert(`Congratulations!\nYou've bought the ${laptops[currentLaptop].manufacturer} ${laptops[currentLaptop].model}`)
});

/* Populate select box */
laptops.forEach(e => {
    const elOpt = document.createElement('option');
    elOpt.text = `${e.manufacturer} ${e.model}`;
    elOpt.value = e.id;
    elSelecter.appendChild(elOpt);
});

/* Display info about selected laptop */
function displayLaptopInfo(id) {
    document.getElementById('laptopImage').src = laptops[id].image;
    document.getElementById('name').innerText = laptops[id].manufacturer + ' ' + laptops[id].model;
    document.getElementById('price').innerText = laptops[id].price + ' Kr.';
    document.getElementById('cpu').innerText = laptops[id].cpuManufacturer + ' ' + laptops[id].cpuModel;
    document.getElementById('ram').innerText = laptops[id].ram;
    document.getElementById('stor').innerText = laptops[id].storage;
    document.getElementById('gpu').innerText = laptops[id].gpu;
    document.getElementById('description').innerText = laptops[id].description;
}
